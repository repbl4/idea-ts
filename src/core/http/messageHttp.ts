const url = 'http://localhost:8081/messages/';

export function getMessage(id: number) {
    return fetch(
            url + id,
            {
                method: 'GET',
                headers: { 'Content-Type': 'application/json' }
            },
        );
}

export async function getMessages(): Promise<IMessage[]>{
    const response = await fetch(
        url,
        {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        },
    );
    return response.json();
}

export async function createMessage(data: IMessage): Promise<IMessage> {
    const response = await fetch(
        url,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
        }
    );
    return response.json();
}

export async function deleteMessage(id: number) {
    const response = await fetch(
        url + id,
        {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        }
    );
    return response.json();
}

export async function updateMessage(id: number, message: IMessage) {
    const response = await fetch(
        url + id,
        {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(message)
        }
    );
    return response.json();
}
