const url = 'http://localhost:8081/users/';

export async function createUser(data: IAuth): Promise<IAuth> {
    const response = await fetch(
        url + 'registration',
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        }
    );
    return response.json();
}
export function getUser(id: number) {
    return fetch(
        url + id,
        {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        },
    );
}
