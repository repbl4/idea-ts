import {createMessage, getMessages} from "../http/messageHttp";


/**
 * Отправка сообщения.
 */
export async function sendMessage() {
    let input = document.getElementsByClassName('form-control')[0] as HTMLInputElement;
    if (input.value !== '') await createMessage({messageNow: input.value});
    await renderLastMessage();
    input.value = '';
}

/**
 * Рендер всех сообщений.
 */

export async function getAllMessages() {
    let div = document.getElementsByClassName('panel')[0];
    const messages = await getMessages();
    if (messages.length !== 0) {
        for (let message of messages) {
            console.log(message.timeCreated);
            div.insertAdjacentHTML('beforeend',
                '<div class="row message-row">' +
                        '<div class="col-sm">'
                            + message.messageNow +
                        '</div>' +
                        '<div class="col-sm" title="' + messages[messages.length - 1].timeCreated + '">'
                            + message.timeCreated +
                        '</div>' +
                    '</div>');
        }
    }
}

export async function renderLastMessage() {
    const messages = await getMessages();
    let input = document.getElementsByClassName('form-control')[0] as HTMLInputElement;
    if (input.value !== '') {
        let div = document.getElementsByClassName('panel')[0];
        console.log(typeof messages[messages.length - 1].timeCreated);
        div.insertAdjacentHTML('beforeend',
            '<div class="row message-row">' +
                    '<div class="col-sm">'
                        + messages[messages.length - 1].messageNow +
                    '</div>' +
                    '<div class="col-sm" title="' + messages[messages.length - 1].timeCreated + '">'
                        + messages[messages.length - 1].timeCreated +
                    '</div>' +
                '</div>');
    }
}




