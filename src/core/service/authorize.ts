import {createUser} from "../http/authHttp";

const registrationWindow =
    '<div class="page-header"><h2>Зарегайся, чорт, сделай это по красоте</h2></div>' +
    '<div class="container">' +
        '<form>' +
            '<div class="form-group">' +
                '<label for="email">Email</label>' +
                '<input type="email" id="email" class="form-control" placeholder="Давай сюда логин">' +
            '</div>' +
            '<div class="form-group">' +
                '<label for="password">Password</label>' +
                '<input type="password" id="password" class="form-control" placeholder="Давай сюда пароль">' +
            '</div>' +
            '<div class="form-group">' +
                '<label for="confirm">Confirm</label>' +
                '<input type="password" id="confirm" class="form-control" placeholder="Давай сюда подтверждение">' +
            '</div>' +
            '<div class="form-group form-check">' +
                '<label class="form-check-label" for="exampleCheck1">Просто галка по приколу</label>' +
                '<input type="checkbox" class="form-check-input" id="exampleCheck1">' +
            '</div>' +
            '<button type="submit" class="btn btn-primary">Тыкай чорт</button>' +
        '</form>' +
    '</div>';

const success = '<div class="page-header"><h1>Поздравляем с успешной регистрацией</h1></div>';

export function renderRegisterPage() {
    const body = document.getElementsByTagName('body')[0] as HTMLBodyElement;
    body.innerHTML = '';
    body.insertAdjacentHTML('afterbegin', registrationWindow);
    const button = document.getElementsByClassName('btn btn-primary')[0] as HTMLButtonElement;
    button.addEventListener('click', function (event) {
        const emailInput = document.getElementById('email') as HTMLInputElement;
        const passInput = document.getElementById('password') as HTMLInputElement;
        const confirmInput = document.getElementById('confirm') as HTMLInputElement;
        const data = {
            email: emailInput.value,
            password: passInput.value,
            confirm: confirmInput.value,
        };
        event.preventDefault();
        createUser(data).then(response => console.log(response));
    });
}
