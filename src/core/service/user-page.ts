export const userData =
    '<div class="page-header"><h2>Личные данные</h2></div>' +
    '<div class="container">' +
        '<form>' +
            '<div class="form-group">' +
                '<label for="login">Login</label>' +
                '<input type="text" id="login" class="form-control" placeholder="Давай сюда логин">' +
            '</div>' +
            '<div class="form-group">' +
                '<label for="first-name">First name</label>' +
                '<input type="text" id="first-name" class="form-control" placeholder="Давай сюда имя">' +
            '</div>' +
            '<div class="form-group">' +
                '<label for="last-name">Last name</label>' +
                '<input type="text" id="last-name" class="form-control" placeholder="Давай сюда фамилию">' +
            '</div>' +
            '<div class="form-group">' +
                '<label for="age">Age</label>' +
                '<input type="text" id="age" class="form-control" placeholder="Давай сюда скок те там">' +
            '</div>' +
            '<button type="submit" class="btn btn-primary">Сохрани же, ну</button>' +
        '</form>' +
    '</div>';
